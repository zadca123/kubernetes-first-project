# Section 4 Deploying an App

## Introduction

In this lab we will be deploying an app to k3s.

## Prerequisite

All of our labs are built upon one another. Please make sure all previous labs are completed before starting this lab.

We are going to also need [nerdctl](https://github.com/containerd/nerdctl).

## Workflow

### Creating a Simple App

To do this we are going to create a directory for our app and call that `hello-world`. Within this directory we need to create a file called `Dockerfile` and another file called `index.html`.

In you favorite text editor open `Dockerfile` and add the following:

``` Dockerfile
FROM registry.suse.com/suse/nginx:latest
COPY . /srv/www/htdocs/
```

Save the file and close it. to build this container will be using [SLE-BCI](https://registry.suse.com/).

Then open the `index.html` file and add the following:

``` html
<h1>Hello World from my k3s cluster!</h1>
```

Save the file and close it.

Now let's build this:

``` bash
nerdctl --namespace k8s.io build --tag hello-world:latest .
```

The output should look something like this:

``` bash
[+] Building 26.4s (7/7)                                   
[+] Building 26.6s (7/7) FINISHED                                                                                                                                                                           
 => [internal] load .dockerignore                                                                                          0.0s
 => => transferring context: 2B                                                                                            0.0s
 => [internal] load build definition from Dockerfile                                                                       0.0s
 => => transferring dockerfile: 106B                                                                                       0.0s
 => [internal] load metadata for registry.suse.com/suse/nginx:latest                                                       9.2s
 => [internal] load build context                                                                                          0.0s
 => => transferring context: 169B                                                                                          0.0s
 => [1/2] FROM registry.suse.com/suse/nginx:latest@sha256:94d8abba261d28744a3cb99a7650463ff102d7828fb4c851b064b4c780db8f76 10.0s
 => => resolve registry.suse.com/suse/nginx:latest@sha256:94d8abba261d28744a3cb99a7650463ff102d7828fb4c851b064b4c780db8f76 0.1s
 => => sha256:b84d51348cf0f6d8b953e9486b1fb5a3c5dbc15ce3268292e9320db8ea6b4f55 18.59MB / 18.59MB                           2.0s
 => => sha256:a252fc4f16171481ff7f731d5b8bad698ed3aab12963b9613e8a67d4047e95dd 47.76MB / 47.76MB                           6.3s
 => => extracting sha256:a252fc4f16171481ff7f731d5b8bad698ed3aab12963b9613e8a67d4047e95dd                                  2.4s
 => => extracting sha256:b84d51348cf0f6d8b953e9486b1fb5a3c5dbc15ce3268292e9320db8ea6b4f55                                  1.2s
 => [2/2] COPY . /usr/share/nginx/html                                                                                     0.5s
 => exporting to docker image format                                                                                       6.5s
 => => exporting layers                                                                                                    0.1s
 => => exporting manifest sha256:c7829341d5f56af6cc0b3cc7d13856b5772f044c106189d093b1b13f5ad989cb                          0.0s
 => => exporting config sha256:317b20d48d282444065f7b86a159efcdb9401521750a8038de92ac986c453098                            0.0s
 => => sending tarball                                                                                                     6.3s
Loaded image: overlayfs@sha256:c7829341d5f56af6cc0b3cc7d13856b5772f044c106189d093b1b13f5ad989cb

```

### Deploying an App

First we are going to need to create a namespace. We have done this in previous courses but this quick command can do it:

``` bash
kubectl create namespace k3s-apps
```

We are going to need to deploy this app and like other courses on Kubernetes we are going to use a `Deployment` to do so. Create a file called `deployment.yaml`.

This `deployment.yaml` file should look like this:

``` yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-world-deployment
  namespace: k3s-apps
  labels:
    type: staticwebapp
spec:
  replicas: 1
  selector:
    matchLabels:
      type: staticwebapp
  template:
    metadata:
      labels:
        type: staticwebapp
    spec:
      containers:
      - name: staticwebapp
        image: hello-world:latest
        imagePullPolicy: Never
        ports:
        - containerPort: 80
        resources:
          requests:
            memory: "32Mi"
            cpu: "200m"
          limits:
            memory: "64Mi"
            cpu: "300m"
```

Now we are going to create this Deployment:

``` bash
kubectl create -f deployment.yaml 
```

Expose our deployment:

``` bash
kubectl expose deployment hello-world-deployment --name hello-world-service --port=8080 --target-port=80 -n k3s-apps
```

We are going to have to deploy and ingress controller:

``` bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.2/deploy/static/provider/cloud/deploy.yaml
```

We will now create an ingress resource declaratively that maps to <your_hostname>.127.0.0.1.sslip.io:

``` bash
kubectl create ingress hello-world-ingress --class=nginx --rule="susembp.127.0.0.1.sslip.io/*=hello-world-service:80" -n k3s-apps
```

Now we need to forward a local port to the ingress controller:

``` bash
kubectl port-forward --namespace=ingress-nginx service/ingress-nginx-controller 8081:80
```

And just like in other labs you can navigate to this `http://<your_hostname>.127.0.0.1.sslip.io:8081` link and then see __Hello World from my k3s cluster!__.
